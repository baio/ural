﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace TourApp.Controllers
{
    public class AppConfigController : Controller
    {

        //
        // GET: /AppConfig/

        public ActionResult Config()
        {
            @ViewBag.ServiceHost = WebConfigurationManager.AppSettings["ServiceHost"];
            @ViewBag.Version = Utils.GetVersion();
            @ViewBag.Env = WebConfigurationManager.AppSettings["Env"];

            return PartialView();
        }

        public ActionResult Manifest()
        {
            @ViewBag.Version = Utils.GetVersion();
            return View();
        }
    }
}
