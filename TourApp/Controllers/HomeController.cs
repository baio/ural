﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace TourApp.Controllers
{
    //
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            @ViewBag.Version = Utils.GetVersion();
            @ViewBag.Env = WebConfigurationManager.AppSettings["Env"];

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
