﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DataAvail.DataService;
using TourApp.Service;

namespace TourApp
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("Service/{*path}");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("App/{*path}");

            routes.MapRoute(
                "Tour", // Route name
                "tour/{*path}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Cabin", // Route name
                "cabin/{*path}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );


            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        public static void RegisterAuthRoutes()
        {
            FormsAuthRouting.MapAuth(
                new [] { 
                    new RouteAuth("/Service/TourService.svc/Filters", RequiredAuth.All),
                    new RouteAuth(".*", RequiredAuth.None)
                });
        }

        protected void Application_Start()
        {
            TourApp.Service.Mapping.CreateMapping();

            AreaRegistration.RegisterAllAreas();

            RegisterAuthRoutes();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            AmqpPublisher.Start();
        }
    }
}