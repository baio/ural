define ["Ural/routerHistory", "Ural/Modules/pubSub"], (routerBase, pubSub) ->

  class Router extends routerBase.RouterBase

    constructor: ->
      super "Controllers", "tour/index"
      @addRoute "tour/filters/{index}", true, (index) =>
        @onControllerRouter "tour", "openFilter", index, null, false
      @addRoute "tour/clientFilters/{index}", true, (index) =>
        @onControllerRouter "tour", "openClientFilter", index, null, false
      @addRoute "tour/requests", true, =>
        @onControllerRouter "tour", "requests", null, null, false
      @addRoute "tour/clientRequests/{index}", true, (index) =>
        @onControllerRouter "tour", "openClientRequest", index, null, false
      @addRoute "tour/filters", true, =>
        @onControllerRouter "tour", "filters", null, null, false
      @addRoute "tour/search", true, =>
        @onControllerRouter "tour", "search", null, null, false
      @addRoute "cabin/exchange", true, =>
        @onControllerRouter "cabin", "exchange", null, null, false
      @addRoute "cabin/board", true, =>
        @onControllerRouter "cabin", "board", null, null, false

    ###
    onRouteChanged: (controller, action) ->
      $(".navbar .nav li.active").removeClass "active"
      $("#nav_menu_#{controller}").addClass "active"
      super controller, action
    ###

    onParseIndex: (controller, action, index) -> index

    getOnLoadingPage: -> "/App/Views/Shared/AppLoad.html"

  Router : Router