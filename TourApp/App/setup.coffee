define [
  "config"
  ,"Models/Zones/Root/root"
  ,"Ural/Modules/socket"
  ,"Ural/Modules/Socket/pusherSocket"
  ,"Ural/Modules/ODataProvider"
  , "Ural/Modules/DataFilterOpts"
  , "Ural/Modules/DataProvider"
  , "Ural/Plugins/tags-binding"
  , "Ural/Plugins/autocomplete-binding"
  , "Ural/Plugins/tooltip-binding"
  , "Ural/Plugins/date-binding"
  , "Ural/Plugins/label-binding"
  , "Ural/Plugins/popover-binding"
  , "Ural/Plugins/badge-binding"
  , "Ural/Plugins/val-binding"
  , "Ural/Plugins/mval-binding"
  , "Models/tag"
  , "Ural/i18n/i18n"
  , "Models/countryInspector"
  , "Models/currInspector"
  , "Models/foodInspector"
  , "bootstrap/js/bootstrap.min"
  , "Ural/Libs/i18n/jquery.ui.datepicker-ru"
  , "Ural/Libs/i18n/moment-ru"
  , "Libs/accounting"
  , "Ural/Plugins/loading-binding"
  , "Ural/Plugins/link-binding"
  ]
  , (config, root, socket, pusherSocket, odataProvider, frOpt, dataProvider, tagsBinding, autocompleteBinding, tooltipBinding
     ,dateBinding, labelBinding, popoverBinding, badgeBinding, valBinding, mvalBinding, tagModel, i18n
     ,countryInspector, currInspector, foodInspector) ->

    frOpt.expandOpts.add null, "$index", ""
    frOpt.expandOpts.add null, "$item", ""
    frOpt.expandOpts.add "Product", "$index", "Tags,Producer"
    frOpt.expandOpts.add "Product", "$item", "Tags,Producer"
    frOpt.expandOpts.add "Producer", "$index", "Products"
    frOpt.expandOpts.add "Producer", "$item", "Products/Tags"
    frOpt.orderBy.add null, "id"
    frOpt.orderBy.add "Tour", "uprice"
    frOpt.orderBy.add "Country", "name"
    frOpt.orderBy.add "Exchange", "date", "desc"
    frOpt.orderBy.add "Food", null
    frOpt.filterOpts.nullRefVal -100500

    window.__g =
      nullRefVal: ->  frOpt.filterOpts.nullRefVal()
      serviceHost : config.serviceHost
      root : new root.Root()

    socket.add "pusher", new pusherSocket.Socket "abad107e940477247b7b"
    socket.set "pusher"

    dataProvider.add "odata", odataProvider.dataProvider
    dataProvider.set "odata"

    _onCompleteCallback = null
    onComplete = (callback) -> _onCompleteCallback = callback
    async.parallel [
      (ck) -> countryInspector.countryInspector.load ck
      (ck) -> currInspector.currInspector.load ck
      (ck) -> foodInspector.foodInspector.load ck
      ]
      , (err) ->
        if err
          throw "error while load setup data : #{err}"
        else
          if _onCompleteCallback
            _onCompleteCallback()
          else
            onComplete = null

    #widgets
    #tags
    tagsBindingOpts =
      tagSource: (req, resp) ->
        dataProvider.get().load req.link, $filter : {name : {$like : req.term}}, (err, data) ->
          if !err
            resp data.map (d) ->
              key : d.id
              label : d.name
              value : d.name
      _parse : (item) ->
        ko.mapping.fromJS {id : item.key, name : item.value}, tagModel.mappingRules, new tagModel.ModelConstructor()
      _format : (item) ->
        key : item.id()
        label : item.name()
        value : item.name()

    tagsBinding.ini tagsBindingOpts

    #autocomplete
    autocompleteOpts =
      source: (req, resp) ->
        dataProvider.get().load req.link, $filter : {name : {$like : req.term}}, (err, data) ->
          if !err
            resp data.map (d) ->
              key : d.id
              label : d.name
              value : d.name
      _parse : (item) ->
        ko.mapping.fromJS {id : item.key, name : item.value}, tagModel.mappingRules, new tagModel.ModelConstructor()
      _format : (item) ->
        key : item.id()
        label : item.name()
        value : item.name()
      _empty : ->
        key : frOpt.filterOpts.nullRefVal()

    autocompleteBinding.ini autocompleteOpts

    #tooltip

    tooltipBinding.ini()
    dateBinding.ini()
    labelBinding.ini()
    popoverBinding.ini()
    badgeBinding.ini()
    valBinding.ini()
    mvalBinding.ini()

    i18n.ini "ru", (data) ->
      __g.i18n = data

    moment.lang "ru"

    onComplete : onComplete
