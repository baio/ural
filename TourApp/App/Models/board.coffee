define ->

  class Board

    constructor: ->

      @id = ko.observable()
      @link = ko.observable()
      @user = ko.observable()
      @msg = ko.observable()
      @type = ko.observable()
      @date = ko.observable()
      @_update = ko.observable(false)

      @duration = ko.computed =>
        @_update()
        moment(@date()).fromNow()

      @ctype = ko.computed =>
        switch @type()
          when "request" then "icon-thumbs-up row-icon-yes"
          when "filter" then "icon-flag row-icon-few"

      @ltype = ko.computed =>
        switch @type()
          when "request" then "запрос"
          when "filter" then "поиск"

    update: -> @_update !@_update()

  ModelConstructor : Board