define [
  "Ural/Modules/DataProvider"
  ,"Models/tag"
],(dataProvider
  , tag
)->

  class CountryInspector

    constructor: ->
      @countries = ko.observableArray()

    load: (onDone) ->
      @_loadCountries onDone

    _loadCountries: (onDone) ->
      dataProvider.get().load "Country", {}, (err, data) =>
        if !err
          for d in data
            t = new tag.ModelConstructor()
            t.id d.id
            t.name d.name
            @countries.push t
        if onDone then onDone err

  countryInspector : new CountryInspector()