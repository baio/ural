define ["Ural/Modules/DataProvider", "Models/tag"],(dataProvider, tag)->

  class HotelInspector

    constructor: ->
      @_country = null
      @_hotels = ko.observableArray()
      @hotels = ko.observableArray()

    load: (country, regions, onDone) ->
      if @_country != country
        regions = []
        @_country = country
        @_loadHotels country, null, =>
          @_selectHotels regions
          if onDone then onDone()
      else
        @_selectHotels regions
        if onDone then onDone()

    _selectHotels: (regions) ->
      if regions.length
        @hotels @_hotels().filter (f) -> regions.indexOf(f.region) != -1
      else
        @hotels @_hotels().map (m) -> m

    _loadHotels: (country, regions, onDone) ->
      regions = if regions then regions.join() else ""
      @_hotels.removeAll()
      dataProvider.get().load "Hotel", $args : { country : country, regions : regions }, (err, data) =>
        if !err
          for d in data
            t = new tag.ModelConstructor()
            t.id d.id
            t.name d.name
            t.label "#{d.name} #{d.stars}*" + if d.rating then " (#{d.rating}})" else "" + " " + if d.region != "unk" then "(#{d.region})" else ""
            t.region = d.region
            @_hotels.push t
        if onDone then onDone err

  hotelInspector : new HotelInspector()