define ["Ural/Modules/DataProvider", "Models/tag"],(dataProvider, tag)->

  class RegionInspector

    constructor: ->
      @regions = ko.observableArray()

    load: (country, onDone) ->
      @regions.removeAll()
      @_loadRegion country, onDone

    _loadRegion: (country, onDone) ->
      dataProvider.get().load "Region", $args : { country : country} , (err, data) =>
        if !err
          for d in data
            t = new tag.ModelConstructor()
            t.id d.id
            t.name d.name
            t.label d.name + " (#{d.count} отеля)"
            @regions.push t
        if onDone then onDone err

  regionInspector : new RegionInspector()