define ->

  class Request
    constructor: ->
      @id = ko.observable()
      @msg = ko.observable().extend
        required: true
        minLength: 5
        maxLength: 120
      @date = ko.observable()
      @tourId = ko.observable()
      ko.mapping.fromJS __metadata.def, __metadata.mapping, @
      ko.validation.validable @

  __metadata =
    mapping : {}
    def :
      id : null
      msg : null
      date : null
      tourId : null

  ModelConstructor : Request
  metadata : __metadata
