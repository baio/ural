define ["hasher", "Ural/Modules/pubSub"], (hasher, pubSub) ->

  class CabinTabs

    constructor: ->

      @active = ko.observable()
      @disabled = ko.observable(false)

      pubSub.subOnce "controller", "action", "cabinTabs", (data) =>
        @active data.action
        @disabled data.begins

  CabinTabs : CabinTabs