// Generated by CoffeeScript 1.3.3
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  define(["Ural/Models/indexVM", "Ural/Modules/ODataProvider"], function(indexVM, pr) {
    var TourFilterStorage;
    TourFilterStorage = (function() {

      function TourFilterStorage(filterPanel, logon) {
        this.filterPanel = filterPanel;
        this.logon = logon;
        this.open = __bind(this.open, this);

        this.showClient = __bind(this.showClient, this);

        this.show = __bind(this.show, this);

        this.store = __bind(this.store, this);

        this.index = new indexVM.IndexVM("Filter");
      }

      TourFilterStorage.prototype.store = function(data, event) {
        var defName;
        if (event) {
          event.preventDefault();
        }
        if (this.filterPanel.checkValid()) {
          if (!this.logon.user()) {
            return this.logon.auth(null, "storeFilter");
          } else {
            defName = "" + (this.filterPanel.country().text()) + " " + (moment(this.filterPanel.dateRange().from()).format("DD.MM")) + " - " + (moment(this.filterPanel.dateRange().to()).format("DD.MM"));
            return this.index.addNew({
              name: defName,
              expr: JSON.stringify(this.filterPanel.expr()),
              exprMeta: JSON.stringify(this.filterPanel.exprMeta())
            });
          }
        }
      };

      TourFilterStorage.prototype.show = function(data, event) {
        var fr, id;
        if (event) {
          event.preventDefault();
        }
        id = typeof data === "string" ? data : data.id();
        if (!this.logon.user()) {
          return this.logon.auth(null, "showFilter/" + id);
        } else {
          fr = this.index.list().filter(function(p) {
            return p.item.id() === id;
          })[0];
          if (fr) {
            return this.filterPanel.restore(JSON.parse(fr.item.expr()), JSON.parse(fr.item.exprMeta()));
          }
        }
      };

      TourFilterStorage.prototype.showClient = function(data, event, onDone) {
        var id,
          _this = this;
        if (event) {
          event.preventDefault();
        }
        id = typeof data === "string" ? data : data.id();
        if (!this.logon.user()) {
          return this.logon.auth(null, "showClientFilter/" + id);
        } else {
          return pr.dataProvider.load("ClientFilter", {
            $filter: {
              id: {
                $eq: id
              }
            }
          }, function(err, data) {
            var fr;
            fr = data[0];
            if (fr) {
              _this.logon.viewModel.zones.state.clientContext.setContext("filter", fr);
              _this.filterPanel.restore(JSON.parse(fr.expr), JSON.parse(fr.exprMeta));
              return onDone({
                opt: "ignore"
              });
            } else {
              err = "Фильтр был удален";
              _this.logon.viewModel.zones.state.clientContext.setContext("error", {
                msg: err
              });
              return onDone({
                msg: err
              });
            }
          });
        }
      };

      TourFilterStorage.prototype.open = function(data, event) {
        var id;
        if (event) {
          event.preventDefault();
        }
        id = typeof data === "string" ? data : data.id();
        return window.open("/tour/filters/" + id, "_blank");
      };

      TourFilterStorage.prototype.load = function(onDone) {
        return this.index.load({}, true, onDone);
      };

      TourFilterStorage.prototype.clear = function() {
        return this.index.removeAll();
      };

      return TourFilterStorage;

    })();
    return {
      TourFilterStorage: TourFilterStorage
    };
  });

}).call(this);
