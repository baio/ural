define ["Ural/Models/indexVM", "Ural/Modules/ODataProvider"], (indexVM, pr) ->

  class TourFilterStorage
    constructor: (@filterPanel, @logon) ->
      @index = new indexVM.IndexVM "Filter"

    store: (data, event) =>
      if event then event.preventDefault()
      if @filterPanel.checkValid()
        if !@logon.user()
          @logon.auth null, "storeFilter"
        else
          defName = "#{@filterPanel.country().text()} #{moment(@filterPanel.dateRange().from()).format "DD.MM"} - #{moment(@filterPanel.dateRange().to()).format "DD.MM"}"
          @index.addNew name : defName, expr : JSON.stringify(@filterPanel.expr()), exprMeta : JSON.stringify(@filterPanel.exprMeta())

    show: (data, event) =>
      if event then event.preventDefault()
      id = if typeof data == "string" then data else data.id()
      if !@logon.user()
        @logon.auth null, "showFilter/#{id}"
      else
        fr = @index.list().filter((p) -> p.item.id() == id)[0]
        if fr
          @filterPanel.restore JSON.parse(fr.item.expr()), JSON.parse(fr.item.exprMeta())

    showClient: (data, event, onDone) =>
      if event then event.preventDefault()
      id = if typeof data == "string" then data else data.id()
      if !@logon.user()
        @logon.auth null, "showClientFilter/#{id}"
      else
        pr.dataProvider.load "ClientFilter", $filter : {id : {$eq : id}}, (err, data) =>
          fr = data[0]
          if fr
            @logon.viewModel.zones.state.clientContext.setContext "filter", fr
            @filterPanel.restore JSON.parse(fr.expr), JSON.parse(fr.exprMeta)
            onDone opt : "ignore"
          else
            err = "Фильтр был удален"
            @logon.viewModel.zones.state.clientContext.setContext "error", msg : err
            onDone msg : err

    open: (data, event) =>
      if event then event.preventDefault()
      id = if typeof data == "string" then data else data.id()
      window.open "/tour/filters/#{id}", "_blank"

    load: (onDone) ->
      @index.load {}, true, onDone

    clear: ->
      @index.removeAll()

  TourFilterStorage : TourFilterStorage