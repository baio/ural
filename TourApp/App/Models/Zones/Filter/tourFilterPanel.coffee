define [
  "Ural/Models/Zones/Filter/filterPanel"
  , "Ural/Models/Zones/Filter/filterRange"
  , "Ural/Models/Zones/Filter/optsList"
  , "Ural/Models/Zones/Filter/optsPairList"
  , "Ural/Models/Zones/Filter/optSingle"
  , "Models/Zones/Filter/priceFilter"
  , "Ural/Models/Zones/Filter/orderByList"
  , "Models/tag"
  , "Models/filter"
  , "Models/countryInspector"
  , "Models/foodInspector"
  , "Models/regionInspector"
  , "Models/hotelInspector"
], (filterPanel
  , filterRange
  , optsList
  , optsPairList
  , optSingle
  , priceFilter
  , orderByList
  , tag
  , filter
  , countryInspector
  , foodInspector
  , regionInspector
  , hotelInspector
) ->

  class TourFilterPanel extends filterPanel.FilterPanel
    constructor: ->
      today = moment("2012-08-30").toDate()
      wtoday = moment().add("d", 7).toDate()
      country = @_countrySerializer().deserialize "at"

      countryFilter = new optSingle.OptSingle "country", countryInspector.countryInspector.countries(), country, @_countrySerializer()

      foodList = foodInspector.foodInspector.food()
      foodFilter = new optsPairList.OptsPairList "food", foodList, null, @_foodSerializer()

      #regionList = regionInspector.regionInspector.regions().filter (f) -> f.id() != "empty"
      regionFilter = new optsPairList.OptsPairList "region", null, null, @_regionSerializer()

      #hotelList = hotelInspector.hotelInspector.hotels().filter (f) -> f.id() != "empty"
      hotelFilter = new optsPairList.OptsPairList "hotel", null, null, @_hotelSerializer(), "name"

      super [
        {name : "dateRange", item : new filterRange.FilterRange "checkin", "checkin", today, wtoday, @_dateSerializer()}
        {name : "nights", item : new filterRange.FilterRange "nights", "nights", 1, 21}
        {name : "country", item : countryFilter}
        {name : "food", item : foodFilter}
        {name : "regions", item : regionFilter}
        {name : "hotels", item : hotelFilter}
        {name : "stars", item : new filterRange.FilterRange "stars", "stars", 1, 5}
        {name : "price", item : new priceFilter.PriceFilter()}
      ],
        new orderByList.OrderByList [
          {key : "eprice", val : "цена"}
          {key : "checkin", val : "дата"}
          {key : "stars desc", val : "звезды"}
          {key : "_crt desc", val : "время добовления"}
        ], "eprice"

      #custom conditions

      @dateRange().from.extend
        dmin : {params : "2012-08-30", message : "Дата заезда (от) не может быть меньше 30 Августа 2012"}
        lte :  @dateRange().to
      @dateRange().to.extend
        dmax : {params : "2012-12-17", message : "Дата заезда (по) не может быть больше 17 Декабря 2012"}

      @nights().from.extend
        lte : @nights().to
      @stars().from.extend
        lte : @stars().to
      @price().from.extend
        lte : @price().to

      ko.validation.validable @

      @descr = ko.observable()
      @descrView = ko.computed =>
        if @descr() then @descr().replace /\[.*?\]/g, "" else null
      @flagIconSrc = ko.observable()
      @_updateCurrent()
      @wasModified = ko.computed =>
        @descr() != filter.ModelConstructor.getDescr @expr(), @exprMeta()

    _loadRegions: (country, onDone) ->
      country ?= @country().selectedOpt().id()
      regionInspector.regionInspector.load country, (err) =>
        list = regionInspector.regionInspector.regions()
        @regions().refresh list, null
        if onDone then onDone err

    _loadHotels: (country, regions, onDone) ->
      country ?= @country().selectedOpt().id()
      regions ?= @regions().selectedOpts().map (m) -> m.id()
      hotelInspector.hotelInspector.load country, regions, (err) =>
        list = hotelInspector.hotelInspector.hotels()
        @hotels().refresh list, null
        if onDone then onDone err

    checkValid: ->
      @dateRange().to.isValid() and
      @dateRange().from.isValid() and
      @nights().from.isValid() and
      @stars().from.isValid() and
      @price().from.isValid()

    change: ->
      if @checkValid()
        super()
        @_updateCurrent()

    _updateCurrent: ->
      @descr filter.ModelConstructor.getDescr @expr(), @exprMeta()
      @flagIconSrc "/App/img/flags/64/#{@country().selectedOpt().id()}.png"

    restore: (fromExpr, fromExprMeta) ->

      @orderBy.select "_crt desc"
      fromExpr = fromExpr.$and
      for f in fromExpr
        for own p of f
          break
        if p == "region"
          f.__meta = name : "regions"
        else if p == "hotel"
          f.__meta = name : "hotels"
        else
          f.__meta = name : p
      dr = fromExpr.filter((f) -> f["checkin"])[0]
      dr ?= fromExpr.filter((f) -> f["checkout"])[0]
      if dr
        dr.__meta =
          name : "dateRange"
      for own p of fromExprMeta
        obj = {}
        obj[p] = fromExprMeta[p]
        obj.__meta = name : p
        fromExpr.push obj
      country = fromExpr.filter((f) -> f["country"])[0].country
      async.waterfall [
        (ck) =>
          @_loadRegions country, ck
        (ck) =>
          @_loadHotels country, null, ck
        ], =>
          @update fromExpr
          console.log "restore"
          @change()

    _foodSerializer: ->

      deserialize: (obj) ->
        foodInspector.foodInspector.food().filter((p) -> p.id() == obj)[0]

      serialize: (val) ->
        val.id()

    _regionSerializer: ->

      deserialize: (obj) ->
        regionInspector.regionInspector.regions().filter((p) -> p.id() == obj)[0]

      serialize: (val) ->
        val.id()

    _hotelSerializer: ->

      deserialize: (obj) ->
        hotelInspector.hotelInspector.hotels().filter((p) -> p.id() == obj)[0]

      serialize: (val) ->
        val.id()

    _countrySerializer: ->
      deserialize: (obj) ->
        country = new tag.ModelConstructor()
        id = if typeof obj  == "string" then obj else obj.id
        country.id id
        if obj.name
          country.name obj.name
        else
          f = countryInspector.countryInspector.countries().filter((p) -> p.id() == id)[0]
          country.name if f then f.name() else obj
        country

      serialize: (val) ->
        id : val.id()
        name : val.name()

    _dateSerializer: ->

      deserialize: (obj)->
        moment(obj).toDate()

      serialize: (val)->
        val

    serialize: ->
      res = super()
      res.push name : "collapse", expr : $(".accordion-body.in").toArray().map((m) -> $(m).attr "id")
      res

    deserialize: (obj, onDone) ->
      c =  obj.filter((i) -> i.name == "country")[0]
      c = c.expr.id if c
      r = obj.filter((i) -> i.name == "regions")[0]
      r = r.expr.vals
      async.parallel [
        (ck) =>
          @_loadRegions c, ck
        (ck) =>
          @_loadHotels c, r, ck
        ] , (err) =>
            super obj, ->
            @_updateCurrent()
            #bug? should subscribe directly to selectedOpt
            @country().expr.subscribe => @_loadRegions()
            @regions().expr.subscribe => @_loadHotels()
            onDone()

  TourFilterPanel : TourFilterPanel