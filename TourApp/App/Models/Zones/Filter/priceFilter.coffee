define ["Models/currInspector"], (currInspector) ->

  class PriceFilter

    constructor: ->
      @from = ko.observable 1
      @to = ko.observable 2000
      @curr = ko.observable "eur"
      @currList = ko.observableArray ["rub","eur","usd"]

      @expr = ko.computed =>
        eprice :
          $gte :  @_getRate @curr(), "eur", @from()
          $lte : @_getRate @curr(), "eur", @to()

    _getRate: (from, to, val) ->
      currInspector.currInspector.convert val, from, to

    update: (fromExpr) ->
      @from fromExpr.price.$gte
      @to fromExpr.price.$lte
      @curr fromExpr.price.curr

    exprMeta: ->
      price :
        $gte :  @from()
        $lte : @to()
        curr : @curr()

    serialize: ->
      @exprMeta()

    deserialize: (obj) ->
      if obj.price
        @update obj

  PriceFilter : PriceFilter