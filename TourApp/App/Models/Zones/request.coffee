define ["Models/request", "Ural/Models/indexVM", "Ural/Modules/pubSub", "Ural/Modules/ODataProvider"], (req
  , indexVM
  , pubSub
  , pr
)->

  class Request

    constructor: (@logon) ->
      @index = new indexVM.IndexVM "Request"
      pubSub.subOnce "request", "create", "request", (tour) =>
        @create tour
      @_createTourMsg = null

    create: (tour) ->
      if typeof tour == "object"
        msg = "Куплю тур, сегодня!\r\n#{tour.lcountry()} #{if tour.region() then tour.region() else ""}, #{tour.hotel()} #{tour.stars()}*, #{tour.fullPrice()}"
        @_createTourMsg = msg
        id = tour.id()
      else
        id = tour
        msg = @_createTourMsg
        @_createTourMsg = null
      if !@logon.user()
        @logon.auth null, "createRequest/#{id}"
      else
        @index.addNew tourId : id, msg : msg

    showClient: (data, event, onDone) =>
      if event then event.preventDefault()
      id = if typeof data == "string" then data else data.id()
      if !@logon.user()
        @logon.auth null, "showClientRequest/#{id}"
      else
        pr.dataProvider.load "ClientRequest", $filter : {id : {$eq : id}}, (err, data) =>
          r = data[0]
          if r
            @logon.viewModel.zones.state.clientContext.setContext "request", r
            @logon.viewModel.load $filter : {id : {$eq : r.tourId}}, onDone
          else
            err = "Запрос был удален"
            @logon.viewModel.zones.state.clientContext.setContext "error", msg : err
            if onDone then onDone msg : err

    load: (onDone) ->
      @index.load {}, true, onDone

    clear: ->
      @index.removeAll()

  Request : Request