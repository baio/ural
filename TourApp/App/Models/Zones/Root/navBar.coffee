define ["Models/Zones/Root/currToolbox", "Ural/Modules/pubSub"], (currToolbox, pubSub) ->

  class NavBar

    constructor: ->

      @currToolbox = new currToolbox.CurrToolbox()

      @disabled = ko.observable(false)

      @active = ko.observable()

      pubSub.sub "controller", "action", (data) =>
        @active data.controller
        @disabled data.begins

  NavBar : NavBar