define ["Models/Zones/Root/navBar"], (navBar) ->

  class Root

    constructor: ->
      @vm = null
      @navBar = new navBar.NavBar()

      #restore
      obj = amplify.store "twitur.toolbox"
      if obj and obj.curr
        @navBar.currToolbox.active obj.curr
      #store
      $(window).unload =>
        amplify.store "twitur.toolbox", curr : @navBar.currToolbox.active()

  Root : Root
