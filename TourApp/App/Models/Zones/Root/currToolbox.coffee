define ["Ural/Modules/pubSub"], (pubSub) ->

  class CurrToolbox

    constructor: ->
      @active = ko.observable "EUR"

      @active.subscribe (val) =>
        console.log "subscribe"
        pubSub.pub "settings", "curr_changed", curr : val

    select: (data, event) =>
      event.preventDefault()
      #@active data
      @active $(event.target).text()

    serialize: ->
      curr : @active()

    deserialize: (obj) ->
      if obj and obj.curr
        @active obj.curr

  CurrToolbox : CurrToolbox