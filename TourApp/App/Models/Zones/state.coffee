define ["Ural/Models/Zones/State"], (state) ->

  class State extends state.State

    constructor: ->
      super()
      @clientContext = new CleintContext()
      @statusText = ko.observable()

  class CleintContext

    constructor: ->
      @type = ko.observable()
      @name = ko.computed => if @type() and @context.user then @context.user else null
      @isInContext = ko.computed => if @name() then true else false
      @sendTweetHref = ko.computed =>
        "https://twitter.com/intent/tweet?screen_name=#{@name()}"
      @title = ko.computed =>
        if @type() == "filter"
          "Фильтр пользователя <a href = '#{@sendTweetHref()}' class='btn btn-small' target='_blank'> @#{@name()}</a> создан #{moment(@context.date).format("DD MMMM, HH : mm")}<br/>"
        else if @type() == "request"
          "Пользователь <a href = '#{@sendTweetHref()}' class='btn btn-small' target='_blank'> @#{@name()}</a> #{moment(@context.date).format("DD MMMM, HH : mm")}, оставил сообщение для этого тура: <br>" +
          "\"#{@context.msg}\""
        else if @type() == "error"
          @context.msg
        else
          null

    setContext: (type, @context) ->
      @type type

  State :  State