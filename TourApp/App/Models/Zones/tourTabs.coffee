define ["Ural/Modules/pubSub"], (pubSub) ->

  class TourTabs

    constructor: ->

      @active = ko.observable()
      @disabled = ko.observable(false)

      pubSub.subOnce "controller", "action", "tourTabs", (data) =>
        #check hash if it is link to some particular item (request or stored filter), show indeed index tab
        if ! /^.*\/.*\/.*\/.*$/.test window.location.pathname
          @active data.action
        else
          @active "index"
        @disabled data.begins

    openStored: (item, storage, event) =>
      event.preventDefault()
      storage.show item
      pubSub.pub "href", "change", href : "tour/index"

    find: (data, event) =>
      event.preventDefault()
      if data.checkValid()
        data.change()
        pubSub.pub "href", "change", href : "tour/index"
        #hasher.setHash "tour/index"

  TourTabs : TourTabs