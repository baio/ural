// Generated by CoffeeScript 1.3.3
(function() {

  define(["Ural/Modules/DataProvider", "Models/tag"], function(dataProvider, tag) {
    var HotelInspector;
    HotelInspector = (function() {

      function HotelInspector() {
        this._country = null;
        this._hotels = ko.observableArray();
        this.hotels = ko.observableArray();
      }

      HotelInspector.prototype.load = function(country, regions, onDone) {
        var _this = this;
        if (this._country !== country) {
          regions = [];
          this._country = country;
          return this._loadHotels(country, null, function() {
            _this._selectHotels(regions);
            if (onDone) {
              return onDone();
            }
          });
        } else {
          this._selectHotels(regions);
          if (onDone) {
            return onDone();
          }
        }
      };

      HotelInspector.prototype._selectHotels = function(regions) {
        if (regions.length) {
          return this.hotels(this._hotels().filter(function(f) {
            return regions.indexOf(f.region) !== -1;
          }));
        } else {
          return this.hotels(this._hotels().map(function(m) {
            return m;
          }));
        }
      };

      HotelInspector.prototype._loadHotels = function(country, regions, onDone) {
        var _this = this;
        regions = regions ? regions.join() : "";
        this._hotels.removeAll();
        return dataProvider.get().load("Hotel", {
          $args: {
            country: country,
            regions: regions
          }
        }, function(err, data) {
          var d, t, _i, _len;
          if (!err) {
            for (_i = 0, _len = data.length; _i < _len; _i++) {
              d = data[_i];
              t = new tag.ModelConstructor();
              t.id(d.id);
              t.name(d.name);
              t.label(("" + d.name + " " + d.stars + "*") + (d.rating ? " (" + d.rating + "})" : "" + " " + (d.region !== "unk" ? "(" + d.region + ")" : "")));
              t.region = d.region;
              _this._hotels.push(t);
            }
          }
          if (onDone) {
            return onDone(err);
          }
        });
      };

      return HotelInspector;

    })();
    return {
      hotelInspector: new HotelInspector()
    };
  });

}).call(this);
