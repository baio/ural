define ["Ural/Modules/DataProvider", "Models/tag"],(dataProvider, tag)->

  class FoodInspector

    constructor: ->
      @food = ko.observableArray()

    load: (onDone) ->

      @_loadFood onDone

    _loadFood: (onDone) ->
      dataProvider.get().load "Food", {}, (err, data) =>
        if !err
          for d in data
            t = new tag.ModelConstructor()
            t.id d.id
            t.name d.name
            t.nameex = d.name + " (#{d.id.toUpperCase()})"
            t.label d.label
            @food.push t
        if onDone then onDone err

    get: (code) ->
      @food().filter((f) -> f.id() == code)[0]

  foodInspector : new FoodInspector()