define ["Ural/Modules/pubSub", "Libs/money"], (pubSub, fx) ->

  fx ?= exports.fx

  currinspector = null

  class CurrInspector

    @DEF_RATES =
      base : "EUR"
      rates :
        USD : 1.2
        RUB : 40

    constructor: ->
      @curr = ko.observable "EUR"
      fx.base = CurrInspector.DEF_RATES.base
      fx.rates = CurrInspector.DEF_RATES.rates
      restoredFx = amplify.store "fx"
      if restoredFx
        fx.base = restoredFx.base
        fx.rates = restoredFx.rates

      setInterval (=> @load()),  30 * 60 * 1000

      pubSub.sub "settings", "curr_changed", (data) =>
        @curr data.curr

    load: (onDone) ->
      @_loadFx (err) ->
        if !err
          amplify.store "fx", {base : fx.base, rates : fx.rates}
        if onDone then onDone err

    formatPrice: (val, curr) ->
      accounting.formatMoney @convert(val, curr, @curr()), { symbol : @curr(), precision :  0, thousand : " ", format : "%v %s" }

    convert: (val, from, to) ->
      res = fx.convert(val, {from : from.toUpperCase(), to : to.toUpperCase()})
      res = Math.round res
      if !res then 1 else res

    _loadFx: (onDone) ->
      $.getJSON('http://openexchangerates.org/latest.json', (data) =>
        if data
          fx.base = data.base
          fx.rates = data.rates
          if onDone then onDone()
        else
          if onDone then onDone err).error (data) ->
            #use defaults
            if onDone then onDone null

  if !currinspector then currinspector = new CurrInspector()

  currInspector : currinspector