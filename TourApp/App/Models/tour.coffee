define ["Ural/Modules/pubSub", "Models/currInspector", "Models/foodInspector"], (pubSub, currInspector, foodInspector) ->

  class Tour
    constructor: ->
      @_fullPriceTrigger = ko.observable()
      @fullPrice = ko.computed
        read: =>
          @_fullPriceTrigger()
          currInspector.currInspector.formatPrice @price(), @curr()
        deferEvaluation: true
      @chs = ko.computed
        read: => @_status2css "hs", @hs()
        deferEvaluation: true
      @ihs = ko.computed
        read: => @_status2info "hs", @hs()
        deferEvaluation: true
      @ctst = ko.computed
        read: => @_status2css "tst", @tst()
        deferEvaluation: true
      @itst = ko.computed
        read: => @_status2info "tst", @tst()
        deferEvaluation: true
      @ctsf = ko.computed
            read: => @_status2css "tsf", @tsf()
            deferEvaluation: true
      @itsf = ko.computed
        read: => @_status2info "tsf", @tsf()
        deferEvaluation: true
      @info = ko.computed
        read: =>
          (if @regionName() then "регион: #{@regionName()}<br/>" else "") +
          "дата добавления: #{moment(@_crt()).format("DD MMMM YYYY HH:mm")}<br/>" +
          "дата обновления: #{moment(@_upd()).format("DD MMMM YYYY HH:mm")}<br/>" +
          "Наличие<ul><li>#{@ihs()}</li><li>#{@itst()}</li><li>#{@itsf()}</li></ul>"
        deferEvaluation: true
      @roomsCountText = ko.computed
        read: =>
          if @suite() then "комнат #{@suite()}" else null
        deferEvaluation: true
      @foodName = ko.computed
        read: =>
          if @food() != "unk"
            t = foodInspector.foodInspector.get(@food())
          if t then  t.name() else @rfood()
        deferEvaluation: true
      @regionName = ko.computed
        read: =>
          if @region() != "unk" then @region() else null
        deferEvaluation: true
      @foodLabel = ko.computed
        read: =>
          res = "Из источника: " + @rfood().toUpperCase()
          if @food() != "unk"
            t = foodInspector.foodInspector.get(@food())
            if t
              res = res + "<br/>" + t.label()
          res
        deferEvaluation: true
      @flagIconSrc = ko.computed
        read: =>
          "/App/img/flags/32/#{@country()}.png"
        deferEvaluation: true

      currInspector.currInspector.curr.subscribe (val) =>
        @_fullPriceTrigger val

    _status2css : (name, val) ->
      ico = switch name
        when "hs" then "icon-home"
        when "tst" then "icon-plane"
        when "tsf" then "icon-plane"
      st = switch val
        when "y" then "row-icon-yes"
        when "r" then "row-icon-request"
        when "s" then "row-icon-few"
        when "n" then "row-icon-no"
        when "?" then "row-icon-unknown"
      ico + " " + st

    _status2info : (name, val) ->
      text = switch name
        when "hs" then "отель - "
        when "tst" then "билеты туда - "
        when "tsf" then "билеты обратно - "
      st = switch val
        when "y" then "в наличии"
        when "r" then "запрос"
        when "s" then "мало"
        when "n" then "нет"
        when "?" then "неизвестно"
      text + " " + st

    send: (data, event) ->
      event.preventDefault()
      if @link
        window.open @link, "_blank"
      else
        pubSub.pub "request", "create", @

  __metadata =
    mapping :
      link :
        create: (opts) ->
          if opts.data then "http://#{opts.data}" else null
      hotellk :
        create: (opts) ->
          if opts.data then "http://#{opts.data}" else null
    def : {}

  ModelConstructor : Tour
  metadata : __metadata