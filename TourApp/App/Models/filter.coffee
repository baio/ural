define ["Models/countryInspector"], (countryInspector) ->

  class Filter
    constructor: ->
      @spanList = ko.observableArray [
        {id : 1, name : "каждый час"}
        {id : 3, name : "каждые 3 часа"}
        {id : 6, name : "каждые 6 часов"}
        {id : 12, name : "раз в день"}
        {id : 24, name : "раз в сутки"}
      ]
      @id = ko.observable()
      @name = ko.observable().extend
        required: true
        minLength: 3
        maxLength: 40
      @expr = ko.observable()
      @exprMeta = ko.observable()
      @notifyby = ko.observable().extend
        required:
          message: "Хотябы одно из значений должно быть выбрано."
      @notifyspan = ko.observable()
      ko.mapping.fromJS __metadata.def, __metadata.mapping, @
      ko.validation.validable @
      @notifybyEmail = ko.computed
        read: => @getNotif 0
        write: (val) => if val then @addNotif(0) else @removeNotif 0
      @notifybyTwitter = ko.computed
        read: => @getNotif 1
        write: (val) => if val then @addNotif 1 else @removeNotif 1
      @flagIconSrc = ko.computed
        read: =>
          cry = JSON.parse(@expr()).$and.filter((f) -> f.country)[0].country
          "/App/img/flags/64/#{cry}.png"
        deferEvaluation : true
      @descr = ko.computed
        read : => Filter.getDescr(JSON.parse(@expr()), JSON.parse(@exprMeta()))
        deferEvaluation: true

    @getDescr: (expr, exprMeta) ->
      f = {}
      for i in expr.$and
        for own p of i
          break
        f[p] = i[p]
      fm = exprMeta
      res =
        countryInspector.countryInspector.countries().filter((p) -> p.id() == f.country)[0].name() + " с " +
        moment(f.checkin.$gte).format("DD MMMM YYYY") + " по " +
        moment(f.checkin.$lte).format("DD MMMM YYYY") + ", ночей от " +
        f.nights.$gte + " до " + f.nights.$lte + ", звезды от " +
        f.stars.$gte + " до " + f.stars.$lte + ", цена от " +
        fm.price.$gte + " до " + fm.price.$lte + " " + fm.price.curr
      if f.food
        f.food.$in.sort()
        res += ", питание (#{f.food.$in.length})[#{f.food.$in.toString()}]"
      if f.region
        f.region.$in.sort()
        res += ", регионы (#{f.region.$in.length})[#{f.region.$in.toString()}]"
      if f.hotel
        f.hotel.$in.sort()
        res += ", отели (#{f.hotel.$in.length})[#{f.hotel.$in.toString()}]"
      res

    getNotif: (val) ->
      if @notifyby().split(",").filter((p) -> parseInt(p) == val)[0] then true else false

    addNotif: (val) ->
      if !@getNotif val
        arr = @notifArray()
        arr.push val
        @notifyby arr.join ","

    removeNotif: (val) ->
      if @getNotif val
        arr = @notifArray()
        _u.removeFromArray arr, val
        @notifyby arr.join ","

    notifArray: -> @notifyby().split(",").filter((p) -> p).map((p) -> parseInt(p))

  __metadata =
    mapping : {}
    def :
      id : null
      name : null
      expr : null
      exprMeta : null
      notifyby : "1"
      notifyspan : 3

  ModelConstructor : Filter
  metadata : __metadata
