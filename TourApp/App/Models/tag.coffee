define ->

  class Tag
    constructor: ->
      @id = ko.observable()
      @name = ko.observable()
      @label = ko.observable()

  ModelConstructor : Tag

