
define ["Controllers/controllerBase", "Ural/Models/Zones/pagerExpander", "Models/Zones/cabinTabs"], (controllerBase, pager, cabinTabs) ->

  class CabinController extends controllerBase.ControllerBase

    constructor: ->
      super null, index : { itemsPerPage : 50, clearOnLoad : false }
      @defaultIndexLayout = "Views/Cabin/Index/Layout"
      @defaultIndexBody = "Views/Cabin/Index/Body"
      @_indexInterval = null

    exchange: (filter, onDone) ->
      @setIndexStreamSocket "exchange_index"
      @_indexInterval = setInterval (=>
        i.item.update() for i in @viewModel.list()), 60000
      @action "exchange", "index", "exchange", filter, onDone

    board: (filter, onDone) ->
      @action "board", "index", "board", filter, onDone

    afterAction: (action) ->
      if action == "exchange"
        @releaseIndexStreamSocket "exchange_index"
        clearInterval @_indexInterval

    showDetails: (id, type, model) ->
      window.open model.link(), "_blank"

    onCreateZones: (context) ->
      super context
      if context == "index"
        @viewModel.zones.tabs = new cabinTabs.CabinTabs()

    onCreatePagerZone: ->
      @viewModel.zones.pager = new pager.Pager @viewModel.list, 200

    onIndexStreamSocket: (data) ->
      date = new Date data.date
      curDate = new Date()
      if date > curDate
        data.date = curDate.toISOString()
      super data

  CabinController : CabinController
