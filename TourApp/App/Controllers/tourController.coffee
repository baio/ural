define [
  "Controllers/controllerBase"
  ,"Models/Zones/Filter/tourFilterPanel"
  ,"Models/Zones/Filter/tourFilterStorage"
  ,"Models/Zones/tourTabs"
  ,"Models/Zones/request"
  ,"Ural/Modules/pubSub"
  ,"Ural/Models/Zones/pagerInfinite"
], (controllerBase
  , tourFilterPanel
  , tourFilterStorage
  , tourTabs
  , request
  , pubSub
  , pager
) ->

  class TourController extends controllerBase.ControllerBase

    constructor: ->
      super "Tour" , index : { itemsPerPage : 20, clearOnPageMove : false  }
      @defaultIndexLayout = "Views/Tour/Index/Layout"
      @defaultIndexBody = "Views/Tour/Index/Body"

    storeFilter: (id) ->
      @filterZone().stored.store()

    showFilter: (id) ->
      @filterZone().stored.show id

    showClientFilter: (id) ->
      @filterZone().stored.showClient id, null, (err) =>
        ###
        if err.opt != "ignore"
          @onLoadEnds err.msg
        ###

    showClientRequest: (id) ->
      #@onLoadBegins()
      @viewModel.zones.request.showClient id, null, (err) =>
        #@onLoadEnds if err then err.msg else null

    createRequest: (id, msg) ->
      @viewModel.zones.request.create id, msg

    openFilter: (id) ->
      @_loadIndex "filters", false, =>
        if !@user()
          @auth "tour/filters/#{id}", "openFilter/#{id}"
        else
          @showFilter id

    openClientFilter: (id) ->
      @_loadIndex "clientFilters", false, =>
        if !@user()
          @auth "tour/clientFilters/#{id}", "openClientFilter/#{id}"
        else
          @showClientFilter id

    openClientRequest: (id) ->
      @_loadIndex "clientRequests", false, =>
        if !@user()
          @auth "tour/clientRequests/#{id}", "openClientRequest/#{id}"
        else
          @showClientRequest id

    onCreateZones: (context) ->
      super context
      if context == "index"
        @viewModel.zones.tabs = new tourTabs.TourTabs()
        @viewModel.zones.request = new request.Request @

    _loadIndex: (action, load, onDone) ->
      if @viewModel
        $(window).scrollTop(0)
        #if action == "index" then @onLoadBegins "index", true
        @onAction action : action, begins : false, (err) ->
          if onDone then onDone err
      else
        @action action, "index", "tour", (if load then null else $skipLoad : true), (err) =>
          #restore collapsed
          obj = @_restore "filter"
          if obj
            collapse = obj.filter((i) -> i.name == "collapse")[0]
            if collapse
              $("#" + c).addClass("in") for c in collapse.expr
          if onDone then onDone err

    index: ->
      @_loadIndex "index", true

    search: (filter) ->
      @_loadIndex "search", true

    filters: (index) ->
      @_loadIndex "filters", true

    requests: (index) ->
      @_loadIndex "requests", true

    onCreateFilterZone: ->
      @toolboxZone().filter = new tourFilterPanel.TourFilterPanel()
      @toolboxZone().filter.stored = new tourFilterStorage.TourFilterStorage @viewModel.zones.toolbox.filter, @

    user: (val) ->
      res = super val
      if val != undefined
        @onLoadIndex null, false
      res

    onLoadUserStuff: (onDone) ->
      if @user()
        async.parallel [
          (ck) =>
            @filterZone().stored.load ck
          (ck) =>
            @viewModel.zones.request.load ck
          ], onDone
      else
        @filterZone().stored.clear()
        @viewModel.zones.request.clear()
        onDone()

    onCreatePagerZone: ->
      @viewModel.zones.pager = new pager.Pager @viewModel.list, 1000, @_checkScrolling

    _checkScrolling: (y, height) =>
      #console.log y + " : " + height + " : " + $(document).height() + " : " + $(window).height()
      $("#index_tab:visible").length and !@stateZone().isLoading() and height - y <= $(window).height() / 1.5

  TourController : TourController