define [
  "Ural/Controllers/controllerAuth"
,"Models/Zones/indexToolbox"
,"Ural/Modules/pubSub"
,"Models/Zones/state"
], (controllerAuth
, indexToolbox
, pubSub
, state
) ->

  class ControllerBase extends controllerAuth.ControllerAuth

    constructor: (type, opts) ->
      super type, opts

    onCreateToolboxZone: ->
      @viewModel.zones.toolbox = new indexToolbox.IndexToolbox()

    onCreateStateZone:  ->
      @viewModel.zones.state = new state.State()

    onLoadBegins: (userFilter, isFiltering) ->
      super userFilter, isFiltering
      @stateZone().loadingStarts = new Date()
      @stateZone().statusText "Загрузка ..."

    onLoadEnds: (err) ->
      super err
      mt = moment @stateZone().loadingStarts
      sec = _u.roundDecimals moment().diff(mt, "seconds", true), 1
      @stateZone().statusText "Загрузка завершена : #{sec} секунд"
      ###
      if !err
        @onShowMessage "info", "Время загрузки #{ sec } секунд", "Загрузка завершена"
      else
        @onShowMessage "error", err, "Ошибка загрузки"
      ###

  ControllerBase : ControllerBase