define ->
  describe "convert framework filter to odata filter expression", ->

    filter = null

    beforeEach ->
      runs ->
        require ["Ural/Modules/ODataFilter"], (fr) -> filter = fr
      waits 500

    it "comlex filter (price like)", ->
      runs ->
        expect(filter).toBeTruthy()
        frameworkFilter =
          $or:[
            {$and:
              [price :
                $gte : 1
                $lte : 2
              currency:
                $eq : "usd"]},
            {$and:[
              price :
                $gte : 3
                $lte : 4
              currency:
                $eq : "eur"]}
          ]
        actual = filter.convert $filter : frameworkFilter
        expected =
          $filter : "((price ge 1 and price le 2 and currency eq 'usd') or (price ge 3 and price le 4 and currency eq 'eur'))"
        expect(actual.$filter).toBe expected.$filter

    it "very simple filter: id = 1", ->
      runs ->
        expect(filter).toBeTruthy()
        frameworkFilter =
          id : {$eq : 1}
        actual = filter.convert $filter : frameworkFilter
        expected =
          $filter : "id eq 1"
        expect(actual.$filter).toBe expected.$filter
        expect(actual.$skip).toBe expected.$skip
        expect(actual.$top).toBe expected.$top

    it "filter: id in (...) and name like (...)", ->
      runs ->
        frameworkFilter =
          $page : 1
          $itemsPerPage : 10
          $filter:
            id : {$in : [0,1,5]}
            name : {$like : "o"}
        actual = filter.convert frameworkFilter
        expected =
          $filter : "(id eq 0 or id eq 1 or id eq 5) and indexof(toupper(name), 'O') ne -1"
          $skip : 0
          $top : 10
        expect(actual.$filter).toBe expected.$filter
        expect(actual.$skip).toBe expected.$skip
        expect(actual.$top).toBe expected.$top

    it "filter: id eq 1 and name eq 'www'", ->
        runs ->
          frameworkFilter =
            $and : [
              id : {$eq : 1}
              name : {$eq : 'www'}
              ]
          actual = filter.convert $filter : frameworkFilter
          expected =
            $filter : "(id eq 1 and name eq 'www')"
          expect(actual.$filter).toBe expected.$filter

    it "filter: date >= [date] and date <= [date]", ->
      runs ->
        frameworkFilter =
          $and :
            [ date : {$lte : moment("01.08.2009", "DD.MM.YYYY").toDate(), $gte : moment("01.07.2009", "DD.MM.YYYY").toDate()} ]
        actual = filter.convert $filter : frameworkFilter
        expected =
          $filter : "(date le DateTime'2009-08-02' and date ge DateTime'2009-07-01')"
        expect(actual.$filter).toBe expected.$filter

    it "filter: range : 0 - 1", ->
      runs ->
        frameworkFilter =
          $and :
            [ {from : {$gte : 0}, to : {$lte : 1}} ]
        actual = filter.convert $filter : frameworkFilter
        expected =
          $filter : "(from ge 0 and to le 1)"
        expect(actual.$filter).toBe expected.$filter