// Generated by CoffeeScript 1.3.3
(function() {

  ({
    toExist: function(expected) {
      return expected !== null && expected !== void 0;
    },
    beforeEach: function() {
      return this.addMatchers({
        toExist: toExist
      });
    }
  });

}).call(this);
