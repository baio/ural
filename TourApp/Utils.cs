﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TourApp
{
    
    public static class Utils
    {
        public static string GetVersion() { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(); }
    }
}