﻿using System;
using System.Collections.Generic;
using System.Data.Services.Common;
using System.Linq;
using System.Web;

namespace TourApp.Service.Models
{
    [Serializable]
    [DataServiceKey("id")]
    public class Exchange
    {
        public int id { get; set; }

        public DateTime date { get; set; }

        public string user { get; set; }

        public string type { get; set; }

        public string msg { get; set; }

        public string link { get; set; }
    }
}