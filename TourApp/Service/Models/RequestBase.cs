﻿using System;
using System.Collections.Generic;
using System.Data.Services.Common;
using System.Linq;
using System.Web;

namespace TourApp.Service.Models
{
    [DataServiceKey("id")]
    public class RequestBase
    {
        public string id { get; set; }

        public string msg { get; set; }

        public System.DateTime? date { get; set; }

        public string tourId { get; set; }
    }
}