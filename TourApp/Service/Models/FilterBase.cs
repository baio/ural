﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Services.Common;

namespace TourApp.Service.Models
{
    [DataServiceKey("id")]
    public class FilterBase
    {
        public string id { get; set; }

        public string name { get; set; }

        public string expr { get; set; }

        public string exprMeta { get; set; }

        //in hours
        public int notifyspan { get; set; }

        /// <summary>
        /// t:twitter_name,e:e_mail
        /// </summary>
        public string notifyby { get; set; }

        public System.DateTime date { get; set; }
    }
}