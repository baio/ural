﻿using System;
using System.Collections.Generic;
using System.Data.Services.Common;
using System.Linq;
using System.Web;

namespace TourApp.Service.Models
{
    [DataServiceKey("id")]
    public class Hotel
    {
        public string id { get; set; }

        public string name { get; set; }

        public int stars { get; set; }

        public int rating { get; set; }

        public string region { get; set; }
    }
}