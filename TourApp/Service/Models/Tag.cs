﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Services.Common;

namespace TourApp.Service.Models
{
    [DataServiceKey("id")]
    public class Tag
    {
        public string id { get; set; }

        public string name { get; set; }
    }

    public class CountryTag : Tag
    {     
    }

    public class FoodTag : Tag
    {
        public string label { get; set; }

        public string code { get; set; }
    }

    public class RegionTag : Tag
    {
        public int count { get; set; }
    }
}