﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TourApp.Service.MongoModels
{
    public class Filter
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public string name { get; set; }

        public string notifyby { get; set; }

        public int notifyspan { get; set; }

        public string expr { get; set; }

        public string exprMeta { get; set; }

        public string user { get; set; }

        public System.DateTime updated { get; set; }

        public System.DateTime date { get; set; }
    }
}