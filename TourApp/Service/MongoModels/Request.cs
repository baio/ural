﻿using System;
using System.Collections.Generic;
using System.Data.Services.Common;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TourApp.Service.MongoModels
{
    [DataServiceKey("id")]
    public class Request
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public string msg { get; set; }

        public System.DateTime? date { get; set; }

        public string tourId { get; set; }

        public string user { get; set; }
    }
}