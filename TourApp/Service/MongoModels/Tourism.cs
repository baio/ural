﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;

namespace TourApp.Service.MongoModels
{
    [BsonIgnoreExtraElements]
    public class Tourism
    {
        public string _id { get; set; }
        public string dom { get; set; }
        public string country { get; set; }
        public System.DateTime? checkin { get; set; }
        public System.DateTime? checkout { get; set; }
        public int? nights { get; set; }
        public string hotel { get; set; }
        public string hotellk { get; set; }
        public int? stars { get; set; }
        public int? suite { get; set; }
        public string suitecat { get; set; }
        public string food { get; set; }
        public int? price { get; set; }
        public string curr { get; set; }

        public string link { get; set; }
        public string region { get; set; }
        public string rsuite { get; set; }
        public string rfood { get; set; }
        public string hs { get; set; }
        public string tst { get; set; }
        public string tsf { get; set; }
        public int? eprice { get; set; }
        public System.DateTime _upd { get; set; }
        public System.DateTime _crt { get; set; }
        public string _parent { get; set; }
        public string _req_id { get; set; }
    }
}