﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TourApp.Service.MongoModels
{
    public class Exchange
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }

        public DateTime date { get; set; }

        public string user { get; set; }

        public string type { get; set; }

        public string msg { get; set; }

        public string link { get; set; }

    }
}