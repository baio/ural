﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using RabbitMQ.Client;

namespace TourApp.Service
{
    internal static class AmqpPublisher
    {
        private static IModel channel = null;

        internal static void Start()
        {
            ConnectionFactory factory = new ConnectionFactory();
            factory.Uri = ConfigurationManager.AppSettings["RABBITMQ_URL"];
            IConnection conn = factory.CreateConnection();
            channel = conn.CreateModel();
            conn.AutoClose = true;
        }

        internal static void Stop()
        {
            channel.Close();
        }

        internal static void Publish(string Str, PublishType PublishType)
        {
            string channelName = null;

            switch(PublishType)
            {
                case Service.PublishType.NewRequest:
                    channelName = "request.new";
                    break;
                case Service.PublishType.NewFilter:
                    channelName = "filter.new";
                    break;
            }

            if (channelName == null) throw new ArgumentOutOfRangeException("PublishType not found");

            var msg = System.Text.Encoding.UTF8.GetBytes(Str);
            IBasicProperties props = channel.CreateBasicProperties();
            props.ContentType = "application/json";
            props.DeliveryMode = 2;
            channel.BasicPublish("", channelName, props, msg);        
        }
    }

    public enum PublishType
    { 
        NewRequest,

        NewFilter
    }
}