﻿using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using DataAvail.DataService;
using System.ServiceModel;
using DataAvail.LinqMapper;
using TourApp.Service;
using TourApp.Service.Models;
using DataAvail.Mvc.Account;
using System.Web.Security;
using MongoDB.Driver;
using FluentMongo.Linq;



namespace TourApp
{
    [JSONPSupportBehavior]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class TourService : DataService<DataServiceProvider>
    {
        // Этот метод вызывается только один раз для инициализации серверных политик.
        public static void InitializeService(DataServiceConfiguration config)
        {
            // TODO: задайте правила, чтобы указать, какие наборы сущностей и служебные операции являются видимыми, обновляемыми и т.д.
            // Примеры:
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
            //config.SetServiceOperationAccessRule("UserFilters", ServiceOperationRights.AllRead);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;            
        }

        /*
        [WebGet]
        public IQueryable<Filter> UserFilters(string Id)
        {
            var user = Membership.GetUser(OAuthUserIdentifyer.CurrentsUserIdentifyer.ServiceIdentifyer.ServiceKey, true);
            var roles = Roles.GetRolesForUser(user.UserName);

            if (!roles.Contains("agent"))
            {
                var q = ((MongoDatabase)this.CurrentDataSource.DataSource).GetCollection<Filter>("filter").AsQueryable();

                if (!string.IsNullOrEmpty(Id))
                { 
                    q = q.Where(p=>p.
                }

                return q;
            }
            else
            { 
                return new Filter[0].AsQueryable();
            }

        }
         */


    }
}

