﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TourApp.Service.Models;
using System.Text.RegularExpressions;
using MongoDB.Driver;
using System.Configuration;

namespace TourApp.Service
{
    public class DataServiceProvider : DataAvail.DataService.Provider.DataServiceProvider
    {
        public DataServiceProvider()
            : base("TourApp.Service.Repos.{0}Repo")
        {
            var connectionString = ConfigurationManager.AppSettings["MONGOLAB_URI"];

            var hostName = Regex.Replace(connectionString, "^(.*)/(.*)$", "$1");
            var dbName = Regex.Replace(connectionString, "^(.*)/(.*)$", "$2");

            var server = MongoServer.Create(hostName);
            server.Connect();
            var db = server.GetDatabase(dbName);

            this.SetContext(db);

            TourApp.Service.i18n.Locale.Current = "ru";
        }

        public IQueryable<Filter> Filters
        {
            get
            {
                return base.CreateQuery<Filter>();
            }
        }

        public IQueryable<ClientFilter> ClientFilters
        {
            get
            {
                return base.CreateQuery<ClientFilter>();
            }
        }

        public IQueryable<Request> Requests
        {
            get
            {
                return base.CreateQuery<Request>();
            }
        }

        public IQueryable<ClientRequest> ClientRequests
        {
            get
            {
                return base.CreateQuery<ClientRequest>();
            }
        }

        public IQueryable<Tour> Tours
        {
            get
            {
                return base.CreateQuery<Tour>();
            }
        }

        public IQueryable<CountryTag> Countries
        {
            get
            {
                return base.CreateQuery<CountryTag>();
            }
        }

        public IQueryable<FoodTag> Foods
        {
            get
            {
                return base.CreateQuery<FoodTag>();
            }
        }

        public IQueryable<Exchange> Exchanges
        {
            get
            {
                return base.CreateQuery<Exchange>();
            }
        }

        public IQueryable<Board> Boards
        {
            get
            {
                return base.CreateQuery<Board>();
            }
        }

        public IQueryable<RegionTag> Regions
        {
            get
            {
                return base.CreateQuery<RegionTag>();
            }
        }

        public IQueryable<Hotel> Hotels
        {
            get
            {
                return base.CreateQuery<Hotel>();
            }
        }

    }
}