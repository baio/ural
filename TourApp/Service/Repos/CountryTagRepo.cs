﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TourApp.Service.Models;
using Microsoft.Data.Services.Toolkit.QueryModel;
using System.Linq.Expressions;
using TourApp.Service.i18n;

namespace TourApp.Service.Repos
{
    public class CountryTagRepo 
    {
        public IEnumerable<CountryTag> GetAll(ODataQueryOperation operation)
        {
            return Locale.GetCountries().Select(p => new CountryTag { id = p.Key, name = p.Value });
        }
    }
}