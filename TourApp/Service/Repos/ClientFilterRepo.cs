﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAvail.DataService.Provider;
using MongoDB.Driver;
using FluentMongo.Linq;
using TourApp.Service.MongoModels;
using DataAvail.Mvc.Account;
using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System.Reflection;
using Microsoft.Data.Services.Toolkit.QueryModel;
using RabbitMQ.Client;
using System.Configuration;
using System.Web.Security;

namespace TourApp.Service.Repos
{
    public class ClientFilterRepo : QRepository<Filter, Models.ClientFilter>, IRepository
    {
        private MongoDatabase _mongoDb = null;

        public void SetContext(object Context)
        {
            _mongoDb = (MongoDatabase)Context; 
        }

        private string User { get { return OAuthUserIdentifyer.CurrentsUserIdentifyer.UserName; } }

        protected override IQueryable<Filter> Queryable
        {
            get 
            {

                if (OAuthUserIdentifyer.CurrentsUserIdentifyer != null)
                {
                    var user = Membership.GetUser(OAuthUserIdentifyer.CurrentsUserIdentifyer.ServiceIdentifyer.ServiceKey, true);
                    var roles = Roles.GetRolesForUser(user.UserName);

                    //if (roles.Contains("agent"))
                    {
                        return _mongoDb.GetCollection<Filter>("filter").AsQueryable();
                    }
                }

                return null;
            }
        }

        protected override string EntityKeyFieldName
        {
            get
            {
                return "_id";
            }
        }
    }
}