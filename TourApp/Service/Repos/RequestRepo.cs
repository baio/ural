﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAvail.DataService.Provider;
using MongoDB.Driver;
using FluentMongo.Linq;
using TourApp.Service.MongoModels;
using DataAvail.Mvc.Account;
using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System.Reflection;
using Microsoft.Data.Services.Toolkit.QueryModel;
using RabbitMQ.Client;
using System.Configuration;

namespace TourApp.Service.Repos
{
    public class RequestRepo : QRepository<Request, Models.Request>, IRepository
    {
        private MongoDatabase _mongoDb = null;

        public void SetContext(object Context)
        {
            _mongoDb = (MongoDatabase)Context; 
        }

        private string User { get { return OAuthUserIdentifyer.CurrentsUserIdentifyer != null ? OAuthUserIdentifyer.CurrentsUserIdentifyer.UserName : null; } }

        protected override IQueryable<Request> Queryable
        {
            get { return _mongoDb.GetCollection<Request>("request").AsQueryable().Where(p => p.user == this.User); }
        }

        public void Save(object Item)
        {
            var request = (Models.Request)Item;

            var coll = _mongoDb.GetCollection<Request>("request");

            var docRequest = Mapper.Map<Request>(request);

            docRequest._id = request.id;
            docRequest.msg = request.msg;
            docRequest.user = User;
            docRequest.date = DateTime.Now;

            coll.Save(docRequest);
            
            request.id = docRequest._id;
            
            AmqpPublisher.Publish((new { docRequest._id, docRequest.msg, user = User }).ToJson(), PublishType.NewRequest);            
        }

        public void Remove(object Item)
        {
            var request = (Models.Request)Item;

            _mongoDb.GetCollection<Request>("request").Remove(Query.EQ("_id", ObjectId.Parse(request.id)));
        }

        protected override string EntityKeyFieldName
        {
            get
            {
                return "_id";
            }
        }
    }
}