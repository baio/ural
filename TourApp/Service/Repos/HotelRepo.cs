﻿using DataAvail.ElasticSearch;
using Microsoft.Data.Services.Toolkit.QueryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TourApp.Service.Models;

namespace TourApp.Service.Repos
{
    public class HotelRepo
    {
        public IEnumerable<Hotel> GetAll(ODataQueryOperation operation)
        {
            /*
            var country = DataServiceProvider.GetQueryStringParam("country").Trim(new[] { '\'' });
            var reg = DataServiceProvider.GetQueryStringParam("regions").Trim(new[] { '\'' });

            if (!string.IsNullOrEmpty(country))
            {                
                var elasticSearch = new ElasticSearch();

                var dsl = elasticSearch.CreateDSL();

                object query = null;

                if (!string.IsNullOrEmpty(reg))
                {
                    var regions = reg.Split(',');

                    query = new { @bool = new { must = new { terms = new { region_key = regions } }, must_1 = new { term = new { country = country } } } };
                }
                else
                {
                    query = new { @bool = new { must = new { term = new { country = country } } } };
                }

                var res = dsl.Query("hotels", "hotel", query);

                return res.Select(p => new Hotel { id = p._id, name = p._source.name, stars = p._source.stars, rating = 0, region = p._source.region_key });
            }
            else
            {
                return new Hotel[0];
            }
             */
            return new Hotel[0];
        }
    }
}