﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAvail.DataService.Provider;
using MongoDB.Driver;
using FluentMongo.Linq;
using TourApp.Service.MongoModels;
using DataAvail.Mvc.Account;
using AutoMapper;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using System.Reflection;
using Microsoft.Data.Services.Toolkit.QueryModel;
using RabbitMQ.Client;
using System.Configuration;
using System.Web.Security;

namespace TourApp.Service.Repos
{
    public class FilterRepo : QRepository<Filter, Models.Filter>, IRepository
    {
        private MongoDatabase _mongoDb = null;

        public void SetContext(object Context)
        {
            _mongoDb = (MongoDatabase)Context; 
        }

        private string User { get { return OAuthUserIdentifyer.CurrentsUserIdentifyer != null ? OAuthUserIdentifyer.CurrentsUserIdentifyer.UserName : null; } }

        protected override IQueryable<Filter> Queryable
        {
            get 
            {                             
                var q = _mongoDb.GetCollection<Filter>("filter").AsQueryable();

                return q.Where(p => p.user == this.User);
            }
        }

        public void Save(object Item)
        {
            var filter = (Models.Filter)Item;

            var coll = _mongoDb.GetCollection<Filter>("filter");

            var docFilter = Mapper.Map<Filter>(filter);

            docFilter._id = filter.id;
            docFilter.updated = System.DateTime.Now;
            docFilter.user = User;
            docFilter.date = System.DateTime.Now;

            foreach (var i in filter.notifyby.Split(',').Select(p => int.Parse(p)))
            { 
                switch(i)
                {
                    case 1:
                        docFilter.notifyby = string.Format("t:{0}", User);
                        break;
                    default:
                        throw new Exception("unsupported notifyby type");
                }
            }
                
            coll.Save(docFilter);

            filter.id = docFilter._id;

            AmqpPublisher.Publish((new { filter.id, filter.name, user = User }).ToJson(), PublishType.NewFilter);                        
        }

        public void Remove(object Item)
        {
            var filter = (Models.Filter)Item;

            _mongoDb.GetCollection<Filter>("filter").Remove(Query.EQ("_id", ObjectId.Parse(filter.id)));
        }

        protected override string EntityKeyFieldName
        {
            get
            {
                return "_id";
            }
        }
    }
}