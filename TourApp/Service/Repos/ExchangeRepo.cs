﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAvail.DataService.Provider;
using Microsoft.Data.Services.Toolkit.QueryModel;
using MongoDB.Driver;
using FluentMongo.Linq;
using TourApp.Service.MongoModels;

namespace TourApp.Service.Repos
{
    public class ExchangeRepo : QRepository<Exchange, Models.Exchange>, IRepository
    {
        private MongoDatabase _mongoDb = null;

        public void SetContext(object Context)
        {
            _mongoDb = (MongoDatabase)Context; 
        }

        protected override IQueryable<Exchange> Queryable
        {
            get
            {
                return _mongoDb.GetCollection<Exchange>("publisher.stream").AsQueryable();
            }
        }
    }
}