﻿using DataAvail.ElasticSearch;
using Microsoft.Data.Services.Toolkit.QueryModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using TourApp.Service.Models;

namespace TourApp.Service.Repos
{
    public class RegionTagRepo
    {
        public IEnumerable<RegionTag> GetAll(ODataQueryOperation operation)
        {
            /*
            var country = DataServiceProvider.GetQueryStringParam("country").Trim(new [] {'\''});

            if (!string.IsNullOrEmpty(country))
            {
                var elasticSearch = new ElasticSearch();

                var dsl = elasticSearch.CreateDSL();

                var res = dsl.Facet<string>("hotels", "hotel", new { term = new { country = country } }, "region_key", "unk");

                return res.Select(p => new RegionTag { id = p.Key, name = p.Key == "unk" ? "неизвестно" : p.Key, count = p.Value });
            }
            else
            {
                return new RegionTag[0];
            }
             */
            return new RegionTag[0];
        }
    }
}