﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TourApp.Service.Models;
using Microsoft.Data.Services.Toolkit.QueryModel;
using System.Configuration;
using System.Text.RegularExpressions;
using MongoDB.Driver;
using TourApp.Service.MongoModels;
using DataAvail.DataService.Provider;
using FluentMongo.Linq;
using TourApp.Service.i18n;
using DataAvail.Mvc.Account;
using System.Web.Security;
using MongoDB.Driver.Builders;
using DataAvail.LinqMapper;
using MongoDB.Bson;
using System.Linq.Expressions;


namespace TourApp.Service.Repos
{
    //
    public class TourRepo : QRepository<Tourism, Tour>, IRepository
    {
        private MongoDatabase _mongoDb = null;

        private string User { get { return OAuthUserIdentifyer.CurrentsUserIdentifyer != null ? OAuthUserIdentifyer.CurrentsUserIdentifyer.UserName : null; } }

        public void SetContext(object Context)
        {
            _mongoDb = (MongoDatabase)Context;
        }

        protected override IQueryable<Tourism> Queryable
        {
            get { return _mongoDb.GetCollection<Tourism>("tourism").AsQueryable().Where(p=>p._parent == null); }
        }

        public override IQueryable<Tour> GetAll(ODataQueryOperation operation)
        {
            var r = ((BinaryExpression)((LambdaExpression)((UnaryExpression)operation.FilterExpression).Operand).Body);

            var vals = new Dictionary<KeyValuePair<string, string>, object>();

            while (r != null)
            {
                var expr = (BinaryExpression)(r.Right is BinaryExpression ? (BinaryExpression)r.Right : r);

                string fieldName = null;

                string oper = null;

                object val = null;

                if (expr.Left is BinaryExpression)
                {
                    fieldName = expr.ToString();

                    var splits = fieldName.Replace("\"", "").Replace("(", "").Replace(")", "")
                        .Split(new[] { "OrElse", "==" }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());

                    if (splits.Contains("it.food"))
                    {
                        fieldName = "food";
                    }

                    if (splits.Contains("it.hotel"))
                    {
                        fieldName = "hotel";
                    }

                    if (splits.Contains("it.region"))
                    {
                        fieldName = "region";
                    }

                    oper = "in";
                    val = splits.Select((p, i) => i % 2 != 0 ? p : null).Where(p => p != null);

                }              
                else if (expr.Left is MemberExpression)
                {
                    fieldName = ((System.Reflection.PropertyInfo)((MemberExpression)expr.Left).Member).Name;

                    val = expr.Right is UnaryExpression ? ((ConstantExpression)((UnaryExpression)expr.Right).Operand).Value : ((ConstantExpression)expr.Right).Value;

                    oper = null;

                    switch (expr.NodeType)
                    {
                        case ExpressionType.LessThanOrEqual:
                            oper = "lte";
                            break;
                        case ExpressionType.GreaterThanOrEqual:
                            oper = "gte";
                            break;
                        case ExpressionType.Equal:
                            oper = "eq";
                            break;
                    }
                }

                vals.Add(new KeyValuePair<string, string>(fieldName, oper), val);

                r = r.Left is BinaryExpression ? (BinaryExpression)r.Left : null;
            }

            var orderByExpr = (ODataOrderExpression)operation.OrderStack.First();

            var r1 = ((LambdaExpression)((UnaryExpression)orderByExpr.Expression).Operand).Body;
            var orderBy = ((System.Reflection.PropertyInfo)((MemberExpression)r1).Member).Name;
            var orderbyAsc = orderByExpr.OrderMethodName == "OrderBy";


            var query = Query.And(
                Query.EQ("country", BsonValue.Create(vals[new KeyValuePair<string, string>("country", "eq")])),
                Query.EQ("_parent", BsonNull.Value),
                Query.GTE("checkin", BsonValue.Create(vals[new KeyValuePair<string, string>("checkin", "gte")])),
                Query.LTE("checkin", BsonValue.Create(vals[new KeyValuePair<string, string>("checkin", "lte")])),
                Query.GTE("nights", BsonValue.Create(vals[new KeyValuePair<string, string>("nights", "gte")])),
                Query.LTE("nights", BsonValue.Create(vals[new KeyValuePair<string, string>("nights", "lte")])),
                Query.GTE("stars", BsonValue.Create(vals[new KeyValuePair<string, string>("stars", "gte")])),
                Query.LTE("stars", BsonValue.Create(vals[new KeyValuePair<string, string>("stars", "lte")])),
                Query.GTE("eprice", BsonValue.Create(vals[new KeyValuePair<string, string>("eprice", "gte")])),
                Query.LTE("eprice", BsonValue.Create(vals[new KeyValuePair<string, string>("eprice", "lte")])));

            //in or equal

            var food = vals.FirstOrDefault(p => p.Key.Key == "food" && (p.Key.Value == "in" || p.Key.Value == "eq")).Value;

            if (food != null)
            {
                if (food.GetType() == typeof(string))
                {
                    query = Query.And(query, Query.EQ("food", BsonValue.Create(food)));
                }
                else
                {
                    query = Query.And(query, Query.In("food", BsonArray.Create(food)));
                }
            }

            var region = vals.FirstOrDefault(p => p.Key.Key == "region" && (p.Key.Value == "in" || p.Key.Value == "eq")).Value;

            if (region != null)
            {
                if (region.GetType() == typeof(string))
                {
                    query = Query.And(query, Query.EQ("region", BsonValue.Create(region)));
                }
                else
                {
                    query = Query.And(query, Query.In("region", BsonArray.Create(region)));
                }
            }


            var hotel = vals.FirstOrDefault(p => p.Key.Key == "hotel" && (p.Key.Value == "in" || p.Key.Value == "eq")).Value;

            if (hotel != null)
            {
                if (hotel.GetType() == typeof(string))
                {
                    query = Query.And(query, Query.EQ("hotel", BsonValue.Create(hotel)));
                }
                else
                {
                    query = Query.And(query, Query.In("hotel", BsonArray.Create(hotel)));
                }
            }
            

            SortByBuilder sortBy = new SortByBuilder();
            if (orderbyAsc)
            {
                sortBy.Ascending(orderBy);
            }
            else
            {
                sortBy.Descending(orderBy);
            }

            var qSrc = _mongoDb.GetCollection<Tourism>("tourism").Find(query).SetSortOrder(sortBy).SetSkip(operation.SkipCount).SetLimit(operation.TopCount).ToArray();

            var q = Mapper.Map<Tourism, Tour>(qSrc.AsQueryable()).ToArray();
             
            var roles = new string[0];

            if (User != null)
            {
                var user = Membership.GetUser(OAuthUserIdentifyer.CurrentsUserIdentifyer.ServiceIdentifyer.ServiceKey, true);
                roles = Roles.GetRolesForUser(user.UserName);
            }

            bool isAgent = roles.Contains("agent");

            foreach (var i in q)
            {
                i.lcountry = Locale.GetCountry(i.country);

                if (!isAgent)
                {
                    i.link = null;
                    i.hotellk = string.Format("google.ru/search?q={0} {1}", i.hotel, i.country);
                }
            }

            operation.FilterExpression = null;
            operation.OrderStack = null;
            operation.SkipCount = 0;
            operation.TopCount = 0;

            return q.AsQueryable();
        }

        protected override string EntityKeyFieldName
        {
            get
            {
                return "_id";
            }
        }

    }
}