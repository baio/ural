﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TourApp.Service.Models;
using Microsoft.Data.Services.Toolkit.QueryModel;
using TourApp.Service.i18n;

namespace TourApp.Service.Repos
{
    public class FoodTagRepo
    {
        public IEnumerable<FoodTag> GetAll(ODataQueryOperation operation)
        {
            return Locale.GetFood().Select(p => new FoodTag { id = p.Key, name = p.Value.Split(';')[0], label = p.Value.Split(';')[1] });
        }
    }
}