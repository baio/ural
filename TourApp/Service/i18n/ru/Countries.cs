﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TourApp.Service.i18n.ru
{
    public static class Countries
    {
        public const string gb = "Англия";
        public const string au = "Австралия";
        public const string @at = "Австрия";
        public const string eg = "Египет";
        public const string vn = "Вьетнам";
        public const string hu = "Венгрия";
        public const string bg = "Болгария";
        public const string de = "Германия";
        public const string gr = "Греция";
        public const string il = "Лихтенштейн";
        public const string jo = "Иордания";
        public const string es = "Испания";
        public const string it = "Италия";
        public const string cy = "Кипр";
        public const string cn = "Китай";
        public const string om = "Оман";
        public const string pt = "Португалия";
        public const string fr = "Франция";
        public const string hr = "Хорватия";
        public const string cs = "Черногория";
        public const string cz = "Чехия";
        public const string ch = "Швейцария";
        public const string tr = "Турция";
        public const string tn = "Тунис";
        public const string th = "Тайланд";
        public const string pl = "Польша";
        public const string dk = "Дания";
        public const string dm = "Доминикана";
        public const string @in = "Индия";
        public const string ie = "Ирландия";
        public const string @is = "Исландия";
        public const string ke = "Кения";
        public const string lv = "Латвия";
        public const string lt = "Литва";
        public const string mx = "Мексика";
        public const string ae = "ОАЭ";
        public const string tz = "Танзания (Мадагаскар)";
        public const string ru = "Россия";
        public const string sk = "Словакия";
        public const string fi = "Финляндия";
        public const string sz = "Свазилэнд";
        public const string se = "Шведция";
        public const string id = "Индонезия";
        public const string @do = "Доминиканская республика";
        public const string ad = "Андора";
        public const string cu = "Куба";
        public const string ee = "Эстония";

    }
}