﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;

namespace TourApp.Service.i18n
{
    public static class Locale
    {
        private static string _current = null;

        private static LocaleTypes _localeTypes = new LocaleTypes(); 

        private class LocaleTypes
        {
            internal Type Countries { get; set; }

            internal Type Food { get; set; }
        }

        private static void UpdateLocaleTypes()
        { 
            //Country

            _localeTypes.Countries = Assembly.GetCallingAssembly().GetType(string.Format("TourApp.Service.i18n.{0}.Countries", Current));
            _localeTypes.Food = Assembly.GetCallingAssembly().GetType(string.Format("TourApp.Service.i18n.{0}.Food", Current));
        }

        public static string Current 
        {
            get { return _current; }

            set 
            {
                if (value != Current)
                {
                    _current = value;

                    UpdateLocaleTypes();
                }
            }
        }

        private static string GetProp(string Type, string PropName)
        {
            FieldInfo fieldInfo = null;

            switch (Type)
            { 
                case "country":
                    fieldInfo = _localeTypes.Countries.GetField(PropName);
                    break;
                case "food":
                    fieldInfo = _localeTypes.Countries.GetField(PropName);
                    break;
            }

            return (string)fieldInfo.GetValue(null);
        }


        public static string GetCountry(string Code)
        {
            return GetProp("country", Code);
        }

        public static string GetFood(string Code)
        {
            return GetProp("food", Code);
        }

        public static IEnumerable<KeyValuePair<string, string>> GetCountries()
        {
            return _localeTypes.Countries.GetFields(BindingFlags.Public | BindingFlags.Static).Select(p => new KeyValuePair<string, string>(p.Name, (string)p.GetValue(null)));
        }

        public static IEnumerable<KeyValuePair<string, string>> GetFood()
        {
            return _localeTypes.Food.GetFields(BindingFlags.Public | BindingFlags.Static).Select(p => new KeyValuePair<string, string>(p.Name, (string)p.GetValue(null)));
        }

    }
}