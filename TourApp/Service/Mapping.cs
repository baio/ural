﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TourApp.Service;
using TourApp.Service.MongoModels;
using DataAvail.LinqMapper;

namespace TourApp.Service
{
    public static class Mapping
    {
        public static void CreateMapping()
        {
            //Tour
            Mapper.CreateMap<Tourism, Models.Tour>()
                .ForMember(p => p.id, opt => opt.MapFrom(p => p._id));

            //Exchange
            Mapper.CreateMap<Exchange, Models.Exchange>()
                .ForMember(p => p.id, opt => opt.MapFrom(p => p._id));

            //Filter
            Mapper.CreateMap<Filter, Models.Filter>()
               .ForMember(p => p.id, opt => opt.MapFrom(p => p._id));

            Mapper.CreateMap<Filter, Models.ClientFilter>()
               .ForMember(p => p.id, opt => opt.MapFrom(p => p._id));

            AutoMapper.Mapper.CreateMap<Models.Filter, Filter>()
                .ForMember(p => p._id, opt => opt.MapFrom(p => p.id));

            //Request
            Mapper.CreateMap<Request, Models.Request>()
               .ForMember(p => p.id, opt => opt.MapFrom(p => p._id));

            Mapper.CreateMap<Request, Models.ClientRequest>()
               .ForMember(p => p.id, opt => opt.MapFrom(p => p._id));

            AutoMapper.Mapper.CreateMap<Models.Request, Request>()
                .ForMember(p => p._id, opt => opt.MapFrom(p => p.id));

        }

    }
}